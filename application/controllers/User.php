<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_data');
        $this->load->helper('url');
    }
    function index() 
    {
        $data['room'] = $this->m_data->tampil_room()->result();
        $this->load->view('header');
        $this->load->view('index', $data);
        $this->load->view('footer'); 
    }
    function jadwal($id) 
    {
        $data['jadwal'] = $this->m_data->tampil_jadwal($id)->result();
        $this->load->view('header');
        $this->load->view('vjadwal', $data);
        $this->load->view('footer'); 
    }

}
