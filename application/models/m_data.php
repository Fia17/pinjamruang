<?php

class m_data extends CI_Model
{
    function tampil_room()
    {
        $this->db
        ->from('waktu_pinjam')
        ->join('ruangan','ruangan.kode_ruangan=waktu_pinjam.kode_ruangan')
        ->join('mahasiswa','mahasiswa.nim=waktu_pinjam.nim');
        $query = $this->db->get();
		return $query;
    }
    function tampil_jadwal($id)
    {
        //$this->db->reset_query()
        $this->db
        ->from('ruangan')
		->join('waktu_pinjam','waktu_pinjam.kode_ruangan=ruangan.kode_ruangan')
        ->where('ruangan.kode_ruangan',$id);
        $query = $this->db->get();
		return $query;
	}
}